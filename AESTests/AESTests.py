import unittest
from copy import copy

import sys
sys.path.append( "../AES" )

from AES import *
from Converter import Converter
from KeyExpander import KeyExpander
from RijndaelHelper import RijndaelHelper
from SBox import SBox

key = Converter.StringToArray("54 68 61 74 73 20 6D 79 20 4B 75 6E 67 20 46 75")
expanded = KeyExpander.ExpandKey(key)
print(Converter.ArrayToString(expanded))

message = Converter.StringToArray("54 77 6F 20 4F 6E 65 20 4E 69 6E 65 20 54 77 6F")
cypher = AES.CypherECB(message, key, padding=False)
print(Converter.ArrayToString(cypher))
    
class AESTests(unittest.TestCase):
    """ """

    def testAddRoundKey(self):
        block = [2,2,3,4,5,6,7,7]
        key = [1,2,3,4,5,6,7,8]
        AES.AddRoundKey(block, key)
        self.assertEqual([3,0,0,0,0,0,0,15], block)


    def testSubBytes(self):
        for i in range(16):
            a = [j for j in range(i*16,i*16+16)]
            b = copy(a)
            AES.SubBytesCypher(a)
            self.assertNotEqual(a, b)
            AES.SubBytesDecypher(a)
            self.assertEqual(a, b)
            AES.SubBytesDecypher(a)
            self.assertNotEqual(a, b)
            AES.SubBytesCypher(a)
            self.assertEqual(a, b)


    def testShiftRowsCypher(self):
        a = [0,1,2,3, 4,5,6,7, 8,9,10,11, 12,13,14,15]
        AES.ShiftRowsCypher(a)
        self.assertEqual([0,1,2,3, 5,6,7,4, 10,11,8,9, 15,12,13,14], a)


    def testShiftRowsDecypher(self):
        a = [0,1,2,3, 4,5,6,7, 8,9,10,11, 12,13,14,15]
        AES.ShiftRowsDecypher(a)
        self.assertEqual([0,1,2,3, 7,4,5,6, 10,11,8,9, 13,14,15,12,], a)


    def testMixColumnsCypher(self):
        data = []
        
        data.append(([142, 142, 142, 142, 77, 77, 77, 77, 161, 161, 161, 161, 188, 188, 188, 188], [219, 219, 219, 219, 19, 19, 19, 19, 83, 83, 83, 83, 69, 69, 69, 69]))
        data.append(([159, 159, 159, 159, 220, 220, 220, 220, 88, 88, 88, 88, 157, 157, 157, 157], [242, 242, 242, 242, 10, 10, 10, 10, 34, 34, 34, 34, 92, 92, 92, 92]))
        data.append(([1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1], [1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1]))
        data.append(([198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198], [198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198]))
        data.append(([213, 213, 213, 213, 213, 213, 213, 213, 215, 215, 215, 215, 214, 214, 214, 214], [212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 213, 213, 213, 213]))
        data.append(([77, 77, 77, 77, 126, 126, 126, 126, 189, 189, 189, 189, 248, 248, 248, 248], [45, 45, 45, 45, 38, 38, 38, 38, 49, 49, 49, 49, 76, 76, 76, 76]))
        data.append(([143, 22, 121, 120, 20, 167, 178, 162, 17, 50, 51, 101, 99, 36, 121, 21], [12, 23, 34, 46, 57, 68, 79, 80, 91, 102, 113, 124, 135, 146, 157, 168]))

        for a,b in data:
            AES.MixColumnsCypher(b)
            self.assertEqual(a, b)


    def testMixColumnsDecypher(self):
        data = []
        
        data.append(([219, 219, 219, 219, 19, 19, 19, 19, 83, 83, 83, 83, 69, 69, 69, 69], [142, 142, 142, 142, 77, 77, 77, 77, 161, 161, 161, 161, 188, 188, 188, 188]))
        data.append(([242, 242, 242, 242, 10, 10, 10, 10, 34, 34, 34, 34, 92, 92, 92, 92], [159, 159, 159, 159, 220, 220, 220, 220, 88, 88, 88, 88, 157, 157, 157, 157]))
        data.append(([1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1], [1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1]))
        data.append(([198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198], [198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198, 198]))
        data.append(([212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 213, 213, 213, 213], [213, 213, 213, 213, 213, 213, 213, 213, 215, 215, 215, 215, 214, 214, 214, 214]))
        data.append(([45, 45, 45, 45, 38, 38, 38, 38, 49, 49, 49, 49, 76, 76, 76, 76], [77, 77, 77, 77, 126, 126, 126, 126, 189, 189, 189, 189, 248, 248, 248, 248]))
        data.append(([12, 23, 34, 46, 57, 68, 79, 80, 91, 102, 113, 124, 135, 146, 157, 168], [143, 22, 121, 120, 20, 167, 178, 162, 17, 50, 51, 101, 99, 36, 121, 21]))

        for a,b in data:
            AES.MixColumnsDecypher(b)
            self.assertEqual(a, b)


    def testCypherDecypherBlock(self):
        pass
        xkey = KeyExpander.ExpandKey(Converter.StringToArray("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"))
        message = Converter.StringToArray("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00")
        cypher = copy(message)
        
        AES.CypherBlock(cypher, xkey)
        self.assertNotEqual(message, cypher)
        AES.DecypherBlock(cypher, xkey)
        self.assertEqual(message, cypher)


    def testCypherECB128(self):
        key = Converter.StringToArray("2b7e151628aed2a6abf7158809cf4f3c")

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("3ad77bb40d7a3660a89ecaf32466ef97"), cypher)

        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("f5d3d58503b9699de785895a96fdbaaf"), cypher)

        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("43b1cd7f598ece23881b00e3ed030688"), cypher)

        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("7b0c785e27e8ad3f8223207104725dd4"), cypher)

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("3ad77bb40d7a3660a89ecaf32466ef97 f5d3d58503b9699de785895a96fdbaaf 43b1cd7f598ece23881b00e3ed030688 7b0c785e27e8ad3f8223207104725dd4"), cypher)


    def testDecypherECB128(self):
        key = Converter.StringToArray("2b7e151628aed2a6abf7158809cf4f3c")

        cypher = Converter.StringToArray("3ad77bb40d7a3660a89ecaf32466ef97")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("f5d3d58503b9699de785895a96fdbaaf")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("43b1cd7f598ece23881b00e3ed030688")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("7b0c785e27e8ad3f8223207104725dd4")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("3ad77bb40d7a3660a89ecaf32466ef97 f5d3d58503b9699de785895a96fdbaaf 43b1cd7f598ece23881b00e3ed030688 7b0c785e27e8ad3f8223207104725dd4")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)


    def testCypherECB192(self):
        key = Converter.StringToArray("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b")

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("bd334f1d6e45f25ff712a214571fa5cc"), cypher)

        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("974104846d0ad3ad7734ecb3ecee4eef"), cypher)

        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("ef7afd2270e2e60adce0ba2face6444e"), cypher)

        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("9a4b41ba738d6c72fb16691603c18e0e"), cypher)

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("bd334f1d6e45f25ff712a214571fa5cc 974104846d0ad3ad7734ecb3ecee4eef ef7afd2270e2e60adce0ba2face6444e 9a4b41ba738d6c72fb16691603c18e0e"), cypher)


    def testDecypherECB192(self):
        key = Converter.StringToArray("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b")

        cypher = Converter.StringToArray("bd334f1d6e45f25ff712a214571fa5cc")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("974104846d0ad3ad7734ecb3ecee4eef")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("ef7afd2270e2e60adce0ba2face6444e")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("9a4b41ba738d6c72fb16691603c18e0e")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("bd334f1d6e45f25ff712a214571fa5cc 974104846d0ad3ad7734ecb3ecee4eef ef7afd2270e2e60adce0ba2face6444e 9a4b41ba738d6c72fb16691603c18e0e")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)


    def testCypherECB256(self):
        key = Converter.StringToArray("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4")

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("f3eed1bdb5d2a03c064b5a7e3db181f8"), cypher)

        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("591ccb10d410ed26dc5ba74a31362870"), cypher)

        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("b6ed21b99ca6f4f9f153e7b1beafed1d"), cypher)

        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("23304b7a39f9f3ff067d8d8f9e24ecc7"), cypher)

        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherECB(message, key, padding=False)
        self.assertEqual(Converter.StringToArray("f3eed1bdb5d2a03c064b5a7e3db181f8 591ccb10d410ed26dc5ba74a31362870 b6ed21b99ca6f4f9f153e7b1beafed1d 23304b7a39f9f3ff067d8d8f9e24ecc7"), cypher)


    def testDecypherECB256(self):
        key = Converter.StringToArray("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4")

        cypher = Converter.StringToArray("f3eed1bdb5d2a03c064b5a7e3db181f8")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("591ccb10d410ed26dc5ba74a31362870")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("b6ed21b99ca6f4f9f153e7b1beafed1d")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("23304b7a39f9f3ff067d8d8f9e24ecc7")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("23304b7a39f9f3ff067d8d8f9e24ecc7")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("f3eed1bdb5d2a03c064b5a7e3db181f8 591ccb10d410ed26dc5ba74a31362870 b6ed21b99ca6f4f9f153e7b1beafed1d 23304b7a39f9f3ff067d8d8f9e24ecc7")
        message = AES.DecypherECB(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)


    def testCypherCBC128(self):
        key = Converter.StringToArray("2b7e151628aed2a6abf7158809cf4f3c")
        
        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("000102030405060708090A0B0C0D0E0F 7649abac8119b246cee98e9b12e9197d"), cypher)
        
        iv = Converter.StringToArray("7649ABAC8119B246CEE98E9B12E9197D")
        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("7649ABAC8119B246CEE98E9B12E9197D 5086cb9b507219ee95db113a917678b2"), cypher)

        iv = Converter.StringToArray("5086CB9B507219EE95DB113A917678B2")
        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("5086CB9B507219EE95DB113A917678B2 73bed6b8e3c1743b7116e69e22229516"), cypher)
        
        iv = Converter.StringToArray("73BED6B8E3C1743B7116E69E22229516")
        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("73BED6B8E3C1743B7116E69E22229516 3ff1caa1681fac09120eca307586e1a7"), cypher)

        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("000102030405060708090A0B0C0D0E0F 7649abac8119b246cee98e9b12e9197d 5086cb9b507219ee95db113a917678b2 73bed6b8e3c1743b7116e69e22229516 3ff1caa1681fac09120eca307586e1a7"), cypher)


    def testDecypherCBC128(self):
        key = Converter.StringToArray("2b7e151628aed2a6abf7158809cf4f3c")
        
        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F 7649abac8119b246cee98e9b12e9197d")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("7649ABAC8119B246CEE98E9B12E9197D 5086cb9b507219ee95db113a917678b2")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("5086CB9B507219EE95DB113A917678B2 73bed6b8e3c1743b7116e69e22229516")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("73BED6B8E3C1743B7116E69E22229516 3ff1caa1681fac09120eca307586e1a7")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F 7649abac8119b246cee98e9b12e9197d 5086cb9b507219ee95db113a917678b2 73bed6b8e3c1743b7116e69e22229516 3ff1caa1681fac09120eca307586e1a7")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)
        

    def testCypherCBC192(self):
        key = Converter.StringToArray("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b")
        
        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("4f021db243bc633d7178183a9fa071e8"), cypher[16:])
        
        iv = Converter.StringToArray("4F021DB243BC633D7178183A9FA071E8")
        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("b4d9ada9ad7dedf4e5e738763f69145a"), cypher[16:])

        iv = Converter.StringToArray("B4D9ADA9AD7DEDF4E5E738763F69145A")
        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("571b242012fb7ae07fa9baac3df102e0"), cypher[16:])
        
        iv = Converter.StringToArray("571B242012FB7AE07FA9BAAC3DF102E0")
        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("08b0e27988598881d920a9e64f5615cd"), cypher[16:])

        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("000102030405060708090A0B0C0D0E0F 4f021db243bc633d7178183a9fa071e8 b4d9ada9ad7dedf4e5e738763f69145a 571b242012fb7ae07fa9baac3df102e0 08b0e27988598881d920a9e64f5615cd"), cypher)


    def testDecypherCBC192(self):
        key = Converter.StringToArray("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b")
        
        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F 4f021db243bc633d7178183a9fa071e8")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("4F021DB243BC633D7178183A9FA071E8 b4d9ada9ad7dedf4e5e738763f69145a")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("B4D9ADA9AD7DEDF4E5E738763F69145A 571b242012fb7ae07fa9baac3df102e0")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("571B242012FB7AE07FA9BAAC3DF102E0 08b0e27988598881d920a9e64f5615cd")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F 4f021db243bc633d7178183a9fa071e8 b4d9ada9ad7dedf4e5e738763f69145a 571b242012fb7ae07fa9baac3df102e0 08b0e27988598881d920a9e64f5615cd")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)
        

    def testCypherCBC256(self):
        key = Converter.StringToArray("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4")
        
        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("f58c4c04d6e5f1ba779eabfb5f7bfbd6"), cypher[16:])
        
        iv = Converter.StringToArray("F58C4C04D6E5F1BA779EABFB5F7BFBD6")
        message = Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("9cfc4e967edb808d679f777bc6702c7d"), cypher[16:])

        iv = Converter.StringToArray("9CFC4E967EDB808D679F777BC6702C7D")
        message = Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("39f23369a9d9bacfa530e26304231461"), cypher[16:])
        
        iv = Converter.StringToArray("39F23369A9D9BACFA530E26304231461")
        message = Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("b2eb05e2c39be9fcda6c19078c6a9d1b"), cypher[16:])

        iv = Converter.StringToArray("000102030405060708090A0B0C0D0E0F")
        message = Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710")
        cypher = AES.CypherCBC(iv, message, key, padding=False)
        self.assertEqual(Converter.StringToArray("000102030405060708090A0B0C0D0E0F f58c4c04d6e5f1ba779eabfb5f7bfbd6 9cfc4e967edb808d679f777bc6702c7d 39f23369a9d9bacfa530e26304231461 b2eb05e2c39be9fcda6c19078c6a9d1b"), cypher)


    def testDecypherCBC256(self):
        key = Converter.StringToArray("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4")
        
        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F f58c4c04d6e5f1ba779eabfb5f7bfbd6")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a"), message)

        cypher = Converter.StringToArray("F58C4C04D6E5F1BA779EABFB5F7BFBD6 9cfc4e967edb808d679f777bc6702c7d")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("ae2d8a571e03ac9c9eb76fac45af8e51"), message)

        cypher = Converter.StringToArray("9CFC4E967EDB808D679F777BC6702C7D 39f23369a9d9bacfa530e26304231461")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("30c81c46a35ce411e5fbc1191a0a52ef"), message)

        cypher = Converter.StringToArray("39F23369A9D9BACFA530E26304231461 b2eb05e2c39be9fcda6c19078c6a9d1b")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("f69f2445df4f9b17ad2b417be66c3710"), message)

        cypher = Converter.StringToArray("000102030405060708090A0B0C0D0E0F f58c4c04d6e5f1ba779eabfb5f7bfbd6 9cfc4e967edb808d679f777bc6702c7d 39f23369a9d9bacfa530e26304231461 b2eb05e2c39be9fcda6c19078c6a9d1b")
        message = AES.DecypherCBC(cypher, key, padding=False)
        self.assertEqual(Converter.StringToArray("6bc1bee22e409f96e93d7e117393172a ae2d8a571e03ac9c9eb76fac45af8e51 30c81c46a35ce411e5fbc1191a0a52ef f69f2445df4f9b17ad2b417be66c3710"), message)

        
def main():
    unittest.main()

if __name__ == '__main__':
    main()
