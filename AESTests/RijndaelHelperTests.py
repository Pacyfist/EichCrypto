import unittest

import sys
sys.path.append( "../AES/" )

from RijndaelHelper import RijndaelHelper

class RijndaelHelperTests(unittest.TestCase):

    def testModulo(self):
        self.assertEqual(0x01, RijndaelHelper.Modulo(0x11A))
        self.assertEqual(0x01, RijndaelHelper.Modulo(0x3F7E))
        self.assertEqual(0x01, RijndaelHelper.Modulo(0x4958))

    def testMultiply(self):
        self.assertEqual(0x01, RijndaelHelper.Multiply(0x53, 0xCA))
        self.assertEqual(0x31, RijndaelHelper.Multiply(0x7, 0xB))
        self.assertEqual(0x01, RijndaelHelper.Multiply(0xE4, 0xC6))

    def testInverse(self):
        self.assertEqual(0xCA, RijndaelHelper.Inverse(0x53))
        self.assertEqual(0x53, RijndaelHelper.Inverse(0xCA))

    def testRcon(self):
        self.assertEqual(0x8D, RijndaelHelper.Rcon(0x00))
        self.assertEqual(0x01, RijndaelHelper.Rcon(0x01))
        self.assertEqual(0x02, RijndaelHelper.Rcon(0x02))
        self.assertEqual(0x04, RijndaelHelper.Rcon(0x03))
        self.assertEqual(0x1B, RijndaelHelper.Rcon(0x09))
        self.assertEqual(0x2F, RijndaelHelper.Rcon(0x10))
        
def main():
    unittest.main()

if __name__ == '__main__':
    main()

