from SBox import SBox
from RijndaelHelper import RijndaelHelper

from copy import copy
import math

class KeyExpander:
    """ """

    @staticmethod
    def Rotate(a):
        result = a[1:]
        result.append(a[0])
        return result

    @staticmethod
    def ExpandKey(key):
        """  """
        #print(key)
        keyx = copy(key)

        if len(key) == 16: #128 bits
            for i in range(40):
                temp = keyx[-4:]

                if i % 4 == 0:
                    temp = KeyExpander.Rotate(temp)

                    temp = [SBox.Cypher(t) for t in temp]

                    temp[0] ^= RijndaelHelper.Rcon(i/4+1)

                for t in temp:
                    keyx.append(t ^ keyx[-16])

        if len(key) == 24: #192 bits
            for i in range(46):
                temp = keyx[-4:]

                if i % 6 == 0:
                    temp = KeyExpander.Rotate(temp)

                    temp = [SBox.Cypher(t) for t in temp]

                    temp[0] ^= RijndaelHelper.Rcon(i/6+1)

                for t in temp:
                    keyx.append(t ^ keyx[-24])

        if len(key) == 32: #256 bits
            for i in range(52):
                temp = keyx[-4:]

                if i % 8 == 0:
                    temp = KeyExpander.Rotate(temp)

                    temp = [SBox.Cypher(t) for t in temp]

                    temp[0] ^= RijndaelHelper.Rcon(i/8+1)

                if i % 8 == 4:
                    temp = [SBox.Cypher(t) for t in temp]

                for t in temp:
                    keyx.append(t ^ keyx[-32])

        return keyx
