from SBox import SBox
from RijndaelHelper import RijndaelHelper
from KeyExpander import KeyExpander

from copy import copy

class AES:
    """ """
    @staticmethod
    def AddRoundKey(block, key):
        for i,(b,k) in enumerate(zip(block,key)):
            block[i] = b^k
        return block

    @staticmethod
    def SubBytesCypher(block):
        for i, b in enumerate(block):
            block[i] = SBox.Cypher(b)

    @staticmethod
    def SubBytesDecypher(block):
        for i, b in enumerate(block):
            block[i] = SBox.Decypher(b)

    @staticmethod
    def ShiftRowsCypher(block):
        for i in range(4):
            row = block[i*4:i*4+4]
            block[i*4:i*4+4] = row[i:] + row[0:i]

    @staticmethod
    def ShiftRowsDecypher(block):
        for i in range(4):
            row = block[i*4:i*4+4]
            block[i*4:i*4+4] = row[-i:] + row[0:-i]

    @staticmethod
    def MixColumnsCypher(block):
        for i in range(4):
            column = [block[4*j+i] for j in range(4)]
            
            block[0 + i] = RijndaelHelper.Multiply(2, column[0]) ^ RijndaelHelper.Multiply(3, column[1]) ^ RijndaelHelper.Multiply(1, column[2]) ^ RijndaelHelper.Multiply(1, column[3])
            block[4 + i] = RijndaelHelper.Multiply(1, column[0]) ^ RijndaelHelper.Multiply(2, column[1]) ^ RijndaelHelper.Multiply(3, column[2]) ^ RijndaelHelper.Multiply(1, column[3])
            block[8 + i] = RijndaelHelper.Multiply(1, column[0]) ^ RijndaelHelper.Multiply(1, column[1]) ^ RijndaelHelper.Multiply(2, column[2]) ^ RijndaelHelper.Multiply(3, column[3])
            block[12 + i] = RijndaelHelper.Multiply(3, column[0]) ^ RijndaelHelper.Multiply(1, column[1]) ^ RijndaelHelper.Multiply(1, column[2]) ^ RijndaelHelper.Multiply(2, column[3])

    @staticmethod
    def MixColumnsDecypher(block):
        for i in range(4):
            column = [block[4*j+i] for j in range(4)]

            block[0 + i] = RijndaelHelper.Multiply(14, column[0]) ^ RijndaelHelper.Multiply(11, column[1]) ^ RijndaelHelper.Multiply(13, column[2]) ^ RijndaelHelper.Multiply(9, column[3])
            block[4 + i] = RijndaelHelper.Multiply(9, column[0]) ^ RijndaelHelper.Multiply(14, column[1]) ^ RijndaelHelper.Multiply(11, column[2]) ^ RijndaelHelper.Multiply(13, column[3])
            block[8 + i] = RijndaelHelper.Multiply(13, column[0]) ^ RijndaelHelper.Multiply(9, column[1]) ^ RijndaelHelper.Multiply(14, column[2]) ^ RijndaelHelper.Multiply(11, column[3])
            block[12 + i] = RijndaelHelper.Multiply(11, column[0]) ^ RijndaelHelper.Multiply(13, column[1]) ^ RijndaelHelper.Multiply(9, column[2]) ^ RijndaelHelper.Multiply(14, column[3])

    @staticmethod
    def MirrorBlock(block):
        temp = copy(block)
        for i in range(4):
            for j in range(4):
                block[(i*4) + j] = temp[((j*4) + i)]
        return block

    @staticmethod
    def CypherBlock(block, xkey):
        """ """              
        AES.AddRoundKey(block, AES.MirrorBlock(xkey[0:16]))
        
        for key in [AES.MirrorBlock(xkey[i:i+16]) for i in range(16, len(xkey)-16, 16)]:
            AES.SubBytesCypher(block)
            AES.ShiftRowsCypher(block)
            AES.MixColumnsCypher(block)
            AES.AddRoundKey(block, key)

        AES.SubBytesCypher(block)
        AES.ShiftRowsCypher(block)
        AES.AddRoundKey(block, AES.MirrorBlock(xkey[-16:]))     

    @staticmethod
    def DecypherBlock(block, xkey):
        """ """
        AES.AddRoundKey(block, AES.MirrorBlock(xkey[-16:]))
        AES.ShiftRowsDecypher(block)
        AES.SubBytesDecypher(block)
         
        for key in [AES.MirrorBlock(xkey[i-16:i]) for i in range(len(xkey)-16, 16, -16)]:
            AES.AddRoundKey(block, key)
            AES.MixColumnsDecypher(block)
            AES.ShiftRowsDecypher(block)
            AES.SubBytesDecypher(block)

        AES.AddRoundKey(block, AES.MirrorBlock(xkey[0:16]))

    @staticmethod
    def AddMessagePadding(message):
        """ """
        padding = 16 - len(message) % 16
        message.extend([padding] * padding)

    @staticmethod
    def DelMessagePadding(message):
        """ """
        padding = message[-1]
        for i in range(padding):
            del(message[-1])

    @staticmethod
    def CypherECB(message, key, padding=True):

        if len(key) not in [16, 24, 32]:
            raise AttributeError("Key - wrong length")

        cypher = []

        xkey = KeyExpander.ExpandKey(key)

        if padding:
            AES.AddMessagePadding(message)
        
        for m in [message[i:i+16] for i in range(0, len(message), 16)]:
            AES.MirrorBlock(m)
            AES.CypherBlock(m, xkey)
            AES.MirrorBlock(m)
            cypher.extend(m)

        return cypher

    @staticmethod
    def DecypherECB(cypher, key, padding=True):

        if len(key) not in [16, 24, 32]:
            raise AttributeError("Key - wrong length")

        message = []

        xkey = KeyExpander.ExpandKey(key)
      
        for c in [cypher[i:i+16] for i in range(0, len(cypher), 16)]:
            AES.MirrorBlock(c)
            AES.DecypherBlock(c, xkey)
            AES.MirrorBlock(c)
            message.extend(c)

        if padding:
            AES.DelMessagePadding(message)

        return message

    @staticmethod
    def CypherCBC(iv, message, key, padding=True):

        if len(iv) != 16:
            raise AttributeError("IV - wrong length")

        if len(key) not in [16, 24, 32]:
            raise AttributeError("Key - wrong length")

        xkey = KeyExpander.ExpandKey(key)

        if padding:
            AES.AddMessagePadding(message)
        
        cypher = copy(iv)

        for m in [message[i:i+16] for i in range(0, len(message), 16)]:
            m = RijndaelHelper.AddLists(cypher[-16:], m)
            AES.MirrorBlock(m)
            AES.CypherBlock(m, xkey)
            AES.MirrorBlock(m)
            cypher.extend(m)

        return cypher

    @staticmethod
    def DecypherCBC(cypher, key, padding=True):

        if len(cypher) < 32:
            raise AttributeError("Cypher - wrong length")

        if len(key) not in [16, 24, 32]:
            raise AttributeError("Key - wrong length")

        if len(cypher)%16 != 0:
            raise AttributeError("Cypher - wrong length")

        xkey = KeyExpander.ExpandKey(key)
    
        message = []

        for p,c in [(cypher[i-16:i], cypher[i:i+16]) for i in range(16, len(cypher), 16)]:
            AES.MirrorBlock(c)
            AES.DecypherBlock(c, xkey)
            AES.MirrorBlock(c)
            c = RijndaelHelper.AddLists(p, c)
            message.extend(c)


        if padding:
            AES.DelMessagePadding(message)

        return message
