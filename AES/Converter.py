class Converter:
    
    @staticmethod
    def StringToArray(string):
        """ """
        string = string.replace(" ","")

        if len(string)%2 == 0:
            return [int(string[i:i+2],16) for i in range(0, len(string), 2)]

        raise ValueError("String has to have even number of elements");

    @staticmethod
    def ArrayToString(key):
        """ """
        string = []
        
        for i in key:
            string.append("{0:02x}".format(i))

        return " ".join(string)
