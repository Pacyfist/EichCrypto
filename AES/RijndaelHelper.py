import math

class RijndaelHelper(object):

    rcon = {}

    @staticmethod
    def AddLists(la, lb):
        result = []
        for (a,b) in zip(la,lb):
            result.append(a^b)
        return result

    @staticmethod
    def Multiply(a, b):
        result = 0;

        for i in range(31):
            bit = (a >> i) & 0x1
            if bit == 0x1:
                result ^= (b << i);

        return RijndaelHelper.Modulo(result);

    @staticmethod
    def Modulo(a):
        """
        x8 + x4 + x3 + x + 1 => 100011011 = 283 = 0x11B
        """
        for i in range(31, 7, -1):
                msb = (a >> i)
                if msb == 0x1:
                    a = a ^ (0x11B << i - 8)
        return a;

    @staticmethod
    def Inverse(a):
        old_s = 0; s = 1; new_s = 0;
        old_t = 0; t = 0; new_t = 1;
        old_r = 0x11B; r = 0x11B; new_r = a;

        while new_r > 0:
            
            r_msb = math.floor(math.log2(r));
            new_r_msb = math.floor(math.log2(new_r));

            quotient = r_msb - new_r_msb;

            if quotient >= 0:
                
                old_s = s;
                s = new_s;
                new_s = old_s ^ (s << quotient);

                old_t = t;
                t = new_t;
                new_t = old_t ^ (t << quotient);

                old_r = r;
                r = new_r;
                new_r = old_r ^ (r << quotient);
                
            else:
                
                new_s = s ^ new_s;
                s = s ^ new_s;
                new_s = s ^ new_s;

                new_t = t ^ new_t;
                t = t ^ new_t;
                new_t = t ^ new_t;

                new_r = r ^ new_r;
                r = r ^ new_r;
                new_r = r ^ new_r;

        if (r > 1):
            return 0;

        return RijndaelHelper.Modulo(t);


    @staticmethod
    def Rcon(a):
        """  """
        if a not in RijndaelHelper.rcon.keys():
            if a == 0:
                RijndaelHelper.rcon[a] = RijndaelHelper.Inverse(0x2)
            else:
                RijndaelHelper.rcon[a] = RijndaelHelper.Modulo(
                        RijndaelHelper.Multiply(RijndaelHelper.Rcon(a-1), 0x2)
                    )
                    
        return RijndaelHelper.rcon[a]
