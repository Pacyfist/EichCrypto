from RijndaelHelper import RijndaelHelper

class SBox:
    """ """
    
    cbox = {}
    dbox = {}
    
    @staticmethod
    def Cypher(value):
            
        if value not in SBox.cbox.keys():
            result = 0;

            inverse = RijndaelHelper.Inverse(value)
            
            for i in range(8):
                row = (0xF1 << i | 0xF1 >> (8 - i))

                t = row & inverse;

                sum = 0;
                for j in range(8):
                    sum ^= (t >> j) & 0x1;

                result |= sum << i;

            result ^= 0x63;

            SBox.cbox[value] = result;
            SBox.dbox[result] = value;
            
        return SBox.cbox[value];
        
    
    @staticmethod
    def Decypher(value):
        
        if value not in SBox.dbox.keys():
            result = 0;

            for i in range(8):
                row = (0xA4 << i | 0xA4 >> (8 - i))

                t = row & value;

                sum = 0;
                for j in range(8):
                    sum ^= (t >> j) & 0x1

                result |= sum << i

            result ^= 0x5

            result = RijndaelHelper.Inverse(result)

            SBox.dbox[value] = result;
            SBox.cbox[result] = value;

        return SBox.dbox[value];

    @staticmethod
    def ClearCache():
        SBox.cbox = {}
        SBox.dbox = {}
